var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// import libraries
var Web3 = require('web3');
var path = require('path');




web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545")); //conanect to local, ganache, geth etc
var MyContractJSON =require(path.join(__dirname, 'build/contracts/TodoList.json'))


// get contract address
contractAddress = MyContractJSON.networks['4002'].address;
console.log("contract_Address==============",contractAddress)
// get abi
const abi = MyContractJSON.abi;

// creating contract object
ToDo = new web3.eth.Contract(abi, contractAddress);

var indexRouter = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
